package net.suby.project.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "net.suby.project")
public class WebConfig extends WebMvcConfigurerAdapter {

    @Bean
    public InternalResourceViewResolver viewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

    // equivalents for <mvc:resources/> tags
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations("/").setCachePeriod(31556926);
        registry.addResourceHandler("/public-resource/**").addResourceLocations("/public-resource/")
                .setCachePeriod(31556926);
        registry.addResourceHandler("/public-resource/app/**").addResourceLocations("/public-resource/app/")
                .setCachePeriod(31556926);
        registry.addResourceHandler("/public-resource/js/**").addResourceLocations("/public-resource/js/")
                .setCachePeriod(31556926);
        registry.addResourceHandler("/public-resource/css/**").addResourceLocations("/public-resource/css/")
                .setCachePeriod(31556926);
        registry.addResourceHandler("/public-resource/images/**")
                .addResourceLocations("/public-resource/images/")
                .setCachePeriod(31556926);
        registry.addResourceHandler("/public-resource/node_modules/**")
                .addResourceLocations("/public-resource/node_modules/").setCachePeriod(31556926);
    }
}
