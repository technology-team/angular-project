package net.suby.project.utils;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 에러 발생시 처리
 *
 * @author 오학섭
 * @since 2017-04-10
 */
@Controller
public class CustomErrorController implements ErrorController {

    private static final String PATH = "/error";

    @RequestMapping(value = PATH)
    public String error() {
        return "/index";
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }
}
