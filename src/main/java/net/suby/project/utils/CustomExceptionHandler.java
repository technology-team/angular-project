package net.suby.project.utils;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * 설명
 *
 * @author 오학섭
 * @since 2017-04-10
 */
@ControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(Exception.class)
    public String handleException(Exception e) {
        System.out.println("CustomException");
        return "Exception";
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public String handle(Exception ex) {
        System.out.println("404");
        return "404";//this is view name
    }
}
