<!DOCTYPE html>
<html>
<head>
    <script>document.write('<base href="' + document.location + '" />');</script>
    <title>Angular QuickStart</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="/public-resource/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="/public-resource/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="/public-resource/css/core.css" rel="stylesheet" type="text/css">
    <link href="/public-resource/css/components.css" rel="stylesheet" type="text/css">
    <link href="/public-resource/css/colors.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Polyfill(s) for older browsers -->
    <script src="/public-resource/node_modules/core-js/client/shim.min.js"></script>

    <script src="/public-resource/node_modules/zone.js/dist/zone.js"></script>
    <script src="/public-resource/node_modules/systemjs/dist/system.src.js"></script>

    <script src="/public-resource/systemjs.config.js"></script>

    <script>
        System.import('/public-resource/main.js').catch(function (err) {
            console.error(err);
        });
    </script>
</head>

<body>
<my-app>Loading AppComponent content here ...</my-app>
</body>
</html>