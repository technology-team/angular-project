"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var dashboard_component_1 = require("./dashboard.component");
var heroes_component_1 = require("./heroes.component");
var hero_detail_component_1 = require("./hero-detail.component");
var app_test_component_1 = require("./app.test.component");
var parent_component_1 = require("./component/nested/parent.component");
var viewchild_component_1 = require("./component/viewchild/viewchild.component");
var contentchild_component_1 = require("./component/contentchild/contentchild.component");
var promise_component_1 = require("./component/promise/promise.component");
var routes = [
    //{path: '', redirectTo: '/dashboard', pathMatch: 'full'},
    { path: '', component: dashboard_component_1.DashboardComponent },
    { path: 'dashboard', component: dashboard_component_1.DashboardComponent },
    { path: 'detail/:id', component: hero_detail_component_1.HeroDetailComponent },
    { path: 'heroes', component: heroes_component_1.HeroesComponent },
    { path: 'nested', component: parent_component_1.NestedParentComponent },
    { path: 'viewchild', component: viewchild_component_1.ViewchildComponent },
    { path: 'contentchild', component: contentchild_component_1.ContentChildComp },
    { path: 'promise', component: promise_component_1.PromiseComponent },
    { path: 'test', component: app_test_component_1.AppTestComponent }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    core_1.NgModule({
        imports: [router_1.RouterModule.forRoot(routes)],
        exports: [router_1.RouterModule]
    })
], AppRoutingModule);
exports.AppRoutingModule = AppRoutingModule;
