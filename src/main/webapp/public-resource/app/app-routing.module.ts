import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {DashboardComponent} from "./dashboard.component";
import {HeroesComponent} from "./heroes.component";
import {HeroDetailComponent} from "./hero-detail.component";
import {AppTestComponent} from "./app.test.component";
import {NestedParentComponent} from "./component/nested/parent.component";
import {ViewchildComponent} from "./component/viewchild/viewchild.component";
import {ContentChildComp} from "./component/contentchild/contentchild.component";
import {PromiseComponent} from "./component/promise/promise.component";

const routes: Routes = [
    //{path: '', redirectTo: '/dashboard', pathMatch: 'full'},
    {path: '', component: DashboardComponent},
    {path: 'dashboard', component: DashboardComponent},
    {path: 'detail/:id', component: HeroDetailComponent},
    {path: 'heroes', component: HeroesComponent},
    {path: 'nested', component: NestedParentComponent},
    {path: 'viewchild', component: ViewchildComponent},
    {path: 'contentchild', component: ContentChildComp},
    {path: 'promise', component: PromiseComponent},
    {path: 'test', component: AppTestComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}