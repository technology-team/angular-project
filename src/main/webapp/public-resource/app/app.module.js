"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
// main(S)
var app_component_1 = require("./app.component");
var app_content_component_1 = require("./component/main/app.content.component");
var app_footer_component_1 = require("./component/main/app.footer.component");
var app_left_component_1 = require("./component/main/app.left.component");
var app_page_header_component_1 = require("./component/main/app.page.header.component");
var app_profile_component_1 = require("./component/main/app.profile.component");
var app_top_component_1 = require("./component/main/app.top.component");
// main(E)
// nested(S)
var parent_component_1 = require("./component/nested/parent.component");
var child_component_1 = require("./component/nested/child.component");
var grandson_component_1 = require("./component/nested/grandson.component");
// nested(E)
// viewchild, contentchild(S)
var viewchild_component_1 = require("./component/viewchild/viewchild.component");
var contentchild_component_1 = require("./component/contentchild/contentchild.component");
// viewchild, contentchild(E)
//promise(S)
var list_component_1 = require("./component/promise/list.component");
var promise_component_1 = require("./component/promise/promise.component");
var mock_service_1 = require("./component/promise/mock.service");
//promise(E)
var dashboard_component_1 = require("./dashboard.component");
var hero_detail_component_1 = require("./hero-detail.component");
var heroes_component_1 = require("./heroes.component");
var hero_service_1 = require("./hero.service");
var app_routing_module_1 = require("./app-routing.module");
var common_1 = require("@angular/common");
var app_test_component_1 = require("./app.test.component");
var app_test_child_component_1 = require("./app.test.child.component");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            forms_1.FormsModule,
            app_routing_module_1.AppRoutingModule
        ],
        declarations: [
            app_component_1.AppComponent,
            app_content_component_1.AppContentComponent,
            app_footer_component_1.AppFooterComponent,
            app_left_component_1.AppLeftComponent,
            app_page_header_component_1.AppPageHeaderComponent,
            app_profile_component_1.AppProfileComponent,
            app_top_component_1.AppTopComponent,
            parent_component_1.NestedParentComponent,
            child_component_1.NestedChildComponent,
            grandson_component_1.NestedGrandsonComponent,
            viewchild_component_1.Item, viewchild_component_1.ItemComponent, viewchild_component_1.ViewchildComponent,
            contentchild_component_1.GroupTitle, contentchild_component_1.childButtonCmp, contentchild_component_1.ButtonGroup, contentchild_component_1.ContentChildComp,
            list_component_1.ListComponent, promise_component_1.PromiseComponent,
            dashboard_component_1.DashboardComponent,
            hero_detail_component_1.HeroDetailComponent,
            heroes_component_1.HeroesComponent,
            app_test_component_1.AppTestComponent,
            app_test_child_component_1.AppChildTestComponent
        ],
        providers: [hero_service_1.HeroService, mock_service_1.MockService,
            { provide: common_1.LocationStrategy, useClass: common_1.HashLocationStrategy } // 해시주소구현
        ],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
