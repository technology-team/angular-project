import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from "@angular/forms";
// main(S)
import {AppComponent} from "./app.component";
import {AppContentComponent} from "./component/main/app.content.component";
import {AppFooterComponent} from "./component/main/app.footer.component";
import {AppLeftComponent} from "./component/main/app.left.component";
import {AppPageHeaderComponent} from "./component/main/app.page.header.component";
import {AppProfileComponent} from "./component/main/app.profile.component";
import {AppTopComponent} from "./component/main/app.top.component";
// main(E)
// nested(S)
import {NestedParentComponent} from "./component/nested/parent.component";
import {NestedChildComponent} from "./component/nested/child.component";
import {NestedGrandsonComponent} from "./component/nested/grandson.component";
// nested(E)
// viewchild, contentchild(S)
import {Item, ItemComponent, ViewchildComponent} from "./component/viewchild/viewchild.component";
import {
    GroupTitle,
    childButtonCmp,
    ButtonGroup,
    ContentChildComp
} from "./component/contentchild/contentchild.component";
// viewchild, contentchild(E)
//promise(S)
import {ListComponent} from "./component/promise/list.component";
import {PromiseComponent} from "./component/promise/promise.component";
import {MockService} from "./component/promise/mock.service";
//promise(E)
import {DashboardComponent} from "./dashboard.component";
import {HeroDetailComponent} from "./hero-detail.component";
import {HeroesComponent} from "./heroes.component";
import {HeroService} from "./hero.service";
import {AppRoutingModule} from "./app-routing.module";
import {LocationStrategy, HashLocationStrategy} from "@angular/common";
import {AppTestComponent} from "./app.test.component";
import {AppChildTestComponent} from "./app.test.child.component";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutingModule
    ],
    declarations: [
        AppComponent,
        AppContentComponent,
        AppFooterComponent,
        AppLeftComponent,
        AppPageHeaderComponent,
        AppProfileComponent,
        AppTopComponent,
        NestedParentComponent,
        NestedChildComponent,
        NestedGrandsonComponent,
        Item, ItemComponent, ViewchildComponent,
        GroupTitle, childButtonCmp, ButtonGroup, ContentChildComp,
        ListComponent, PromiseComponent,
        DashboardComponent,
        HeroDetailComponent,
        HeroesComponent,
        AppTestComponent,
        AppChildTestComponent
    ],
    providers: [HeroService, MockService,
        {provide: LocationStrategy, useClass: HashLocationStrategy}     // 해시주소구현
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}