import {Component, Input} from "@angular/core";

@Component({
    selector: 'child-test',
    templateUrl: './app.test.child.component.html'
})
export class AppChildTestComponent {
    @Input() title: string;
}