"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var AppLeftComponent = (function () {
    function AppLeftComponent() {
    }
    AppLeftComponent.prototype.ngAfterViewInit = function () {
        var locationHash = window.location.hash;
        var url = locationHash.slice(2);
        document.getElementById(url).className = "active";
    };
    AppLeftComponent.prototype.activeMenu = function (actionName) {
        var ul = document.getElementById("navigationId");
        var items = ul.getElementsByTagName("li");
        for (var i = 0; i < items.length; ++i) {
            if (items[i].className == "navigation-header") {
                continue;
            }
            items[i].className = "";
        }
        document.getElementById(actionName).className = "active";
    };
    return AppLeftComponent;
}());
AppLeftComponent = __decorate([
    core_1.Component({
        selector: 'app-left',
        templateUrl: './app.left.component.html',
        styles: ["\n        .active2 {background-color:#fff;}\n    "]
    })
], AppLeftComponent);
exports.AppLeftComponent = AppLeftComponent;
