import {Component, AfterViewInit} from "@angular/core";

@Component({
    selector: 'app-left',
    templateUrl: './app.left.component.html',
    styles: [`
        .active2 {background-color:#fff;}
    `]
})

export class AppLeftComponent implements AfterViewInit {
    ngAfterViewInit() {     // 리플래시할 경우 메뉴 활성화
        let locationHash = window.location.hash;
        var url: string = locationHash.slice(2);
        document.getElementById(url).className = "active";
    }

    activeMenu(actionName: string) {    // 메뉴 class 변경
        var ul = document.getElementById("navigationId");
        var items = ul.getElementsByTagName("li");
        for (var i = 0; i < items.length; ++i) {
            if (items[i].className == "navigation-header") {
                continue;
            }
            items[i].className = "";
        }
        document.getElementById(actionName).className = "active";
    }

}