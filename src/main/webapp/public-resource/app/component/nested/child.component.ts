import {Component, Input, Output, EventEmitter} from "@angular/core";

@Component({
    selector: 'nested-child',
    templateUrl: './child.component.html'
})

export class NestedChildComponent {
    @Input() fruit: string;
    @Input() fruit2: string;
    @Input() fruit3: string[];
    @Input() fruit5: string;
    @Input() fruit6: string;

    active = false;
    @Output() outputProperty = new EventEmitter<boolean>();

    updateParent(active: boolean) {
        this.active = !active;
        this.outputProperty.emit(this.active);
    }
}