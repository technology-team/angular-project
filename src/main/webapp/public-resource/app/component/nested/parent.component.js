"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var NestedParentComponent = (function () {
    function NestedParentComponent() {
        this.fruit = "사과";
        this.fruit3 = ["딸기", "포도", "참외"];
    }
    NestedParentComponent.prototype.fruit2 = function () {
        return "배";
    };
    Object.defineProperty(NestedParentComponent.prototype, "fruit6", {
        get: function () {
            return "";
        },
        enumerable: true,
        configurable: true
    });
    NestedParentComponent.prototype.outputEvent = function (active) {
        this.active = active;
    };
    return NestedParentComponent;
}());
NestedParentComponent.fruit5 = "수박"; // static형은 곧바로 전달 불가
NestedParentComponent = __decorate([
    core_1.Component({
        selector: 'nested-parent',
        templateUrl: './parent.component.html'
    })
], NestedParentComponent);
exports.NestedParentComponent = NestedParentComponent;
