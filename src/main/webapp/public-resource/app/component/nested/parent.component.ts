import {Component} from "@angular/core";

@Component({
    selector: 'nested-parent',
    templateUrl: './parent.component.html'
})

export class NestedParentComponent {
    fruit = "사과";

    fruit2() {
        return "배";
    }

    fruit3 = ["딸기", "포도", "참외"];

    static fruit5 = "수박";    // static형은 곧바로 전달 불가

    get fruit6() {
        return "";
    }

    active: boolean;

    outputEvent(active: boolean) {
        this.active = active;
    }
}